import { v4 as uuidv4 } from "uuid";

export const CART_DATA = [
  {
    id: uuidv4(),
    key: "Products Count",
    description: "Max Products Count",
    value: {
      type: "number",
      initialValue: 10,
    },
    isChecked: false,
  },
  {
    id: uuidv4(),
    key: "Products Type",
    description: "Gives Product Type information",
    value: {
      type: "text",
      initialValue: "",
    },
    isChecked: false,
  },
  {
    id: uuidv4(),
    key: "Listing Count",
    description: "Number of listings per product",
    value: {
      type: "number",
      initialValue: 3,
    },
    isChecked: false,
  },
  {
    id: uuidv4(),
    key: "Geo Browser",
    description: "Get zone level ordering of product",
    value: {
      type: "option",
      initialValue: "disabled",
      allValues: ["disabled", "enabled"],
    },
    isChecked: false,
  },
  {
    id: uuidv4(),
    key: "Client Tag",
    description: "Client Tag",
    value: {
      type: "text",
      initialValue: "mobile-apps-retail",
    },
    isChecked: false,
  },
  {
    id: uuidv4(),
    key: "Pincode",
    description: "Pincode to search in",
    value: {
      type: "text",
      initialValue: "560103",
    },
    isChecked: false,
  },
  {
    id: uuidv4(),
    key: "Disable Cache",
    description: "Disable augmentation cache",
    value: {
      type: "option",
      initialValue: "true",
      allValues: ["true", "false"],
    },
    isChecked: true,
  },
];
