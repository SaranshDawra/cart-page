import React from "react";
import Row from "./Row";
import "./table.css";

const Table = ({
  data,
  setData,
  checkedFilter,
  searchFilter,
}) => {
  let table = "";
  if (checkedFilter && searchFilter.trim() !== "") {
    const tempData = data.filter((item) => {
      return item.isChecked;
    });

    const str = searchFilter.trim();
    table = tempData.map((item) => {
      if (item.description.toLowerCase().includes(str.toLowerCase())) {
        return (
          <Row
            key={item.id}
            item={item}
            data={data}
            setData={setData}
          />
        );
      } else {
        return null;
      }
    });
  } else if (checkedFilter) {
    table = data.map((item) => {
      if (item.isChecked) {
        return (
          <Row
            key={item.id}
            item={item}
            data={data}
            setData={setData}
          />
        );
      } else {
        return null;
      }
    });
  } else if (searchFilter.trim() !== "") {
    const str = searchFilter.trim();
    table = data.map((item) => {
      if (item.description.toLowerCase().includes(str.toLowerCase())) {
        return (
          <Row
            key={item.id}
            item={item}
            data={data}
            setData={setData}
          />
        );
      } else {
        return null;
      }
    });
  } else {
    table = data.map((item) => {
      return (
        <Row
          key={item.id}
          item={item}
          data={data}
          setData={setData}
        />
      );
    });
  }

  return (
    <table>
      <thead>
        <tr>
          <th>Key</th>
          <th>Value</th>
          <th>Description</th>
        </tr>
      </thead>
      <tbody>{table}</tbody>
    </table>
  );
};

export default Table;
