import React from "react";
import Value from "../../Value";
import CheckBox from "../../Checkbox";

const Row = ({ item, data, setData }) => {
  const checkedHandler = (id) => {
    const tempData = data.map((item) => {
      if (item.id === id) {
        item.isChecked = !item.isChecked;
      }
      return item;
    });
    setData(tempData);
  };

  const valueChangeHandler = (event, id) => {
    const tempData = data.map((item) => {
      if (id === item.id) {
        item.value.initialValue =
          item.value.type === "number"
            ? Number(event.target.value)
            : event.target.value;
      }

      return item;
    });
    setData(tempData);
  };

  return (
    <tr>
      <td>
        <CheckBox
          id="rowCheck"
          checked={item.isChecked}
          onClick={() => checkedHandler(item.id)}
        />
        {item.key}
      </td>
      <td>
        <Value
          type={item.value.type}
          value={item.value.initialValue}
          id={item.id}
          onChange={(event) => valueChangeHandler(event, item.id)}
          options={item.value.type === "option" ? item.value.allValues : null}
        />
      </td>
      <td>{item.description}</td>
    </tr>
  );
};

export default Row;
