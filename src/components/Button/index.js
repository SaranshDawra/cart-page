import React from "react";
import "./button.css";

const Button = ({ title, onClick }) => {

  return (
    <div onClick={() => onClick()} className="btn" data-testid={title}>
      {title}
    </div>
  );
};

export default Button;
