import React from "react";

const CheckBox = ({ checked, onClick, id }) => {
  return (
      <input
        data-testid={id}
        type="checkbox"
        className="checkbox"
        checked={checked}
        onChange={onClick}
      />
  );
};

export default CheckBox;
