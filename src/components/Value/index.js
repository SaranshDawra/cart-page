import React from "react";

const Value = (props) => {
  const { type, value, id, onChange, options, placeholder = "" } = props;

  const element = (() => {
    if (type === "number") {
      return (
        <input
          data-testid={id}
          type="number"
          value={value}
          onChange={onChange}
          placeholder={placeholder}
        />
      );
    } else if (type === "text") {
      return (
        <input
          data-testid={id}
          type="text"
          value={value}
          onChange={onChange}
          placeholder={placeholder}
        />
      );
    } else if (type === "option") {
      const selectElement = options.map((item) => {
        return <option key={item}>{item}</option>;
      });
      return (
        <select data-testid={id} onChange={onChange}>
          {selectElement}
        </select>
      );
    } else {
      return null;
    }
  })();

  return element;
};

export default Value;
