import React, { useState } from "react";
import CheckBox from "../components/Checkbox";

const CustomCheckboxRenderer = ({ id }) => {
  const [checked, setChecked] = useState(false);
  return (
    <CheckBox id={id} checkedd={checked} onClick={() => setChecked(!checked)} />
  );
};

export default CustomCheckboxRenderer;
