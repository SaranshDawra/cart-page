import React, { useState } from "react";
import Value from "../components/Value";

const CustomValueRenderer = ({ type, initVal, options }) => {
  const [value, setValue] = useState(initVal);

  return (
    <Value
      value={value}
      onChange={(event) => setValue(event.target.value)}
      id={101}
      type={type}
      options={type === "option" ? options : null}
    />
  );
};

export default CustomValueRenderer;
