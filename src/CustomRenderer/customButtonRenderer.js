import React, { useState } from 'react';
import Button from '../components/Button';

const CustomButtonRenderer = () => {
  const [title, setTitle] = useState("Done");

  return <Button title={title} onClick={() => setTitle("Testing Complete")}/>;

};

export default CustomButtonRenderer;