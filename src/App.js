import React, { useState } from "react";
import Table from "./components/Table";
import Button from "./components/Button";
import CheckBox from "./components/Checkbox";
import Value from "./components/Value";
import { CART_DATA } from "./constants/data";
import "./App.css";

function App() {
  const [data, setData] = useState(CART_DATA);
  const [checkedFilter, setCheckedFilter] = useState(false);
  const [searchFilter, setSearchFilter] = useState("");

  const handleSubmit = () => {
    const loggingData = data.map((item) => {
      const obj = {
        id: item.id,
        key: item.key,
        value: item.value.initialValue,
        description: item.description,
      };
      return obj;
    });

    console.log(loggingData);
  };

  return (
    <div className="App">
      <div className="filters">
        <div className="searchFilter">
          <Value
            value={searchFilter}
            type="text"
            onChange={(e) => setSearchFilter(e.target.value)}
            id="search"
            placeholder="Search"
          />
        </div>
        <div className="checkedFilter">
          <CheckBox
            id="selected"
            onClick={() => setCheckedFilter((prevState) => !prevState)}
            checked={checkedFilter}
          />
          <label htmlFor="selected">Show Selected</label>
        </div>
      </div>
      <Table
        data={data}
        setData={setData}
        checkedFilter={checkedFilter}
        searchFilter={searchFilter}
      />
      <hr></hr>
      <div className="btn-container">
        <Button title="Done" onClick={handleSubmit} />
      </div>
    </div>
  );
}

export default App;
