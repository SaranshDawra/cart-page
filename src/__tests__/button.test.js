import React from "react";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Button from "../components/Button";
import CustomButtonRenderer from "../CustomRenderer/customButtonRenderer";

describe("button component tests", () => {
  test("should render button", () => {
    const title = "Done";
    const { getByText } = render(<Button title={title} />);
    expect(getByText(title)).toBeInTheDocument();
  });

  test("button onClick 1", () => {
    const { getByText } = render(<CustomButtonRenderer />);
    userEvent.click(getByText("Done"));
    expect(getByText("Testing Complete")).toBeInTheDocument();
  });

  test("button onClick 2", () => {
    const handleSubmit = jest.fn();
    const {getByText} = render(<Button title="Done" onClick={handleSubmit}/>);
    userEvent.click(getByText("Done"));
    expect(handleSubmit).toBeCalled();
  });
});
