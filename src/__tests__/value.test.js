import React from "react";
import { fireEvent, render } from "@testing-library/react";
import Value from "../components/Value";
import CustomValueRenderer from "../CustomRenderer/customValueRenderer";

describe("value component tests", () => {
  test("should render input", () => {
    const handleSubmit = jest.fn();
    const { getByTestId } = render(
      <Value type="number" value={10} id={101} onChange={handleSubmit} />
    );
    expect(getByTestId("101")).toBeInTheDocument();
  });

  test("input value change: number", () => {
    const { getByTestId } = render(
      <CustomValueRenderer type="number" initVal={10} />
    );
    expect(getByTestId("101").value).toBe("10");
    fireEvent.change(getByTestId("101"), { target: { value: "20" } });
    expect(getByTestId("101").value).toBe("20");
    fireEvent.change(getByTestId("101"), { target: { value: "TEST" } });
    expect(getByTestId("101").value).not.toBe("TEST");
  });

  test("input value change: text", () => {
    const { getByTestId } = render(
      <CustomValueRenderer type="text" initVal="TEST" />
    );
    expect(getByTestId("101").value).toBe("TEST");
    fireEvent.change(getByTestId("101"), { target: { value: "20" } });
    expect(getByTestId("101").value).toBe("20");
  });

  test("input value change: select", () => {
    const options = ["disabled", "enabled"];
    const { getByTestId } = render(
      <CustomValueRenderer type="option" initVal="disabled" options={options} />
    );
    expect(getByTestId("101").value).toBe("disabled");
    fireEvent.change(getByTestId("101"), { target: { value: "enabled" } });
    expect(getByTestId("101").value).toBe("enabled");
    expect(getByTestId("101").value).not.toBe("TEST");
  });
});
