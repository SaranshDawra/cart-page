import React from "react";
import { cleanup, render, screen } from "@testing-library/react";
import Table from "../components/Table";

const mockData = [
  {
    id: 1,
    key: "Products Count",
    description: "Max Products Count",
    value: {
      type: "number",
      initialValue: 10,
    },
    isChecked: false,
  },
  {
    id: 2,
    key: "Products Type",
    description: "Gives Product Type information",
    value: {
      type: "text",
      initialValue: "",
    },
    isChecked: false,
  },
  {
    id: 3,
    key: "Listing Count",
    description: "Number of listings per product",
    value: {
      type: "number",
      initialValue: 3,
    },
    isChecked: false,
  },
  {
    id: 4,
    key: "Geo Browser",
    description: "Get zone level ordering of product",
    value: {
      type: "option",
      initialValue: "disabled",
      allValues: ["disabled", "enabled"],
    },
    isChecked: false,
  },
  {
    id: 5,
    key: "Client Tag",
    description: "Client Tag",
    value: {
      type: "text",
      initialValue: "mobile-apps-retail",
    },
    isChecked: false,
  },
  {
    id: 6,
    key: "Pincode",
    description: "Pincode to search in",
    value: {
      type: "text",
      initialValue: "560103",
    },
    isChecked: false,
  },
  {
    id: 7,
    key: "Disable Cache",
    description: "Disable augmentation cache",
    value: {
      type: "option",
      initialValue: "true",
      allValues: ["true", "false"],
    },
    isChecked: false,
  },
];

describe("table component tests", () => {

  test("should render all rows (no filter)", () => {
    const setData = jest.fn();
    const { container } = render(
      <Table
        data={mockData}
        setData={setData}
        checkedFilter={false}
        searchFilter=""
      />
    );
    const table = container.firstChild;
    expect(table.rows.length).toBe(mockData.length + 1);
  });

  test("should render all match from description (search filter)", () => {
    const setData = jest.fn();
    const { container } = render(
      <Table
        data={mockData}
        setData={setData}
        checkedFilter={false}
        searchFilter="product"
      />
    );
    const table = container.firstChild;
    expect(table.rows.length).toBe(5);
  });

  test("should render all checked rows (checked filter)", () => {
    const setData = jest.fn();
    const data = mockData.map((item, idx) => {
      if (idx % 2 === 0) {
        const obj = {
          ...item,
          isChecked: true,
        };
        return obj;
      } else {
        const obj = {
          ...item,
        };
        return obj;
      }
    });
    const { container } = render(
      <Table
        data={data}
        setData={setData}
        checkedFilter={true}
        searchFilter=""
      />
    );
    const table = container.firstChild;
    expect(table.rows.length).toBe(5);
  });

  test("should render rows with both filter applied ", () => {
    const setData = jest.fn();
    const data = mockData.map((item, idx) => {
      if (idx % 2 === 0) {
        const obj = {
          ...item,
          isChecked: true,
        };
        return obj;
      } else {
        const obj = {
          ...item,
        };
        return obj;
      }
    });
    const { container } = render(
      <Table
        data={data}
        setData={setData}
        checkedFilter={true}
        searchFilter="product"
      />
    );
    const table = container.firstChild;
    expect(table.rows.length).toBe(3);
  });
});
