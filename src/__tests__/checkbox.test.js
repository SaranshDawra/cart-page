import React from "react";
import { fireEvent, render } from "@testing-library/react";
import CheckBox from "../components/Checkbox";
import CustomCheckboxRenderer from "../CustomRenderer/customCheckboxRenderer";

describe("checkbox component tests", () => {
  test("should render checkbox", () => {
    const handleSubmit = jest.fn();
    const { getByTestId } = render(
      <CheckBox id="rowCheck" checked={false} onClick={handleSubmit} />
    );
    expect(getByTestId("rowCheck")).toBeDefined();
  });

  test("check/uncheck onChange", () => {
    const { getByTestId } = render(<CustomCheckboxRenderer id="rowCheck" />);
    expect(getByTestId("rowCheck").checked).toBeFalsy();
    fireEvent.click(getByTestId("rowCheck"));
    expect(getByTestId("rowCheck").checked).toBeTruthy();
  });
});
