import React from "react";
import { fireEvent, render } from "@testing-library/react";
import Row from "../components/Table/Row";

const mockData = [
  {
    id: 1,
    key: "Products Count",
    description: "Max Products Count",
    value: {
      type: "number",
      initialValue: 10,
    },
    isChecked: false,
  },
  {
    id: 2,
    key: "Products Type",
    description: "Gives Product Type information",
    value: {
      type: "text",
      initialValue: "",
    },
    isChecked: false,
  },
  {
    id: 3,
    key: "Geo Browser",
    description: "Get zone level ordering of product",
    value: {
      type: "option",
      initialValue: "disabled",
      allValues: ["disabled", "enabled"],
    },
    isChecked: false,
  },
];

describe("table row tests", () => {
  test("should render table row", () => {
    const setData = jest.fn();
    const { container } = render(
      <table>
        <tbody>
          <Row data={mockData} setData={setData} item={mockData[0]} />
        </tbody>
      </table>
    );
    const table = container.querySelector("table");
    expect(table).toBeInTheDocument();
    const tbody = container.querySelector("tbody");
    expect(tbody).toBeInTheDocument();
  });

  test("setData should be called once on input change", () => {
    const setData = jest.fn();
    const { getByTestId } = render(
      <table>
        <tbody>
          <Row data={mockData} setData={setData} item={mockData[0]} />
        </tbody>
      </table>
    );
    fireEvent.change(getByTestId("1"), { target: { value: "20" } });
    expect(setData).toHaveBeenCalledTimes(1);
  });

  test("setData should be called once on checkbox click", () => {
    const setData = jest.fn();
    const { getByTestId } = render(
      <table>
        <tbody>
          <Row data={mockData} setData={setData} item={mockData[0]} />
        </tbody>
      </table>
    );
    fireEvent.click(getByTestId("rowCheck"));
    expect(setData).toHaveBeenCalledTimes(1);
  });
});
